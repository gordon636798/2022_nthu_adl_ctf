#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

struct donate_bar *donate_bars[3];

void init()
{
    setvbuf(stdin, NULL, _IONBF, 0);
    setvbuf(stdout, NULL, _IONBF, 0);
    setvbuf(stderr, NULL, _IONBF, 0);
}

struct donate_bar
{
    void (*func)();
    char *name;
};

void clear_donate(int i)
{
    free(donate_bars[i]->name);
    free(donate_bars[i]);
}

void say()
{
    printf("Thnak you, ");
}

void add_donate(int i, int size)
{
    donate_bars[i] = (struct donate_bar *)malloc(sizeof(struct donate_bar));
    donate_bars[i]->func = say;
    donate_bars[i]->name = (char *)malloc(size);
}

int read_int()
{
    char buf[0x10];
    read(0, buf, 0x10);
    return atoi(buf);
}

void magic_func()
{
    system("/bin/sh");
}

int main()
{
    int opt, index, size;

    init();

    while (1)
    {
        puts("1. Add donate");
        puts("2. Say donate");

        printf("option > ");
        opt = read_int();

        if (opt == 1)
        {
            printf("input index(1~3) > ");
            index = read_int();

            if (index >= 1 || index <= 3)
            {
                printf("input size of your name > ");
                size = read_int();
                add_donate(index, size);
                printf("input your name > ");
                read(0, donate_bars[index]->name, size);
            }
        }
        else if (opt == 2)
        {
            printf("input index(1~3) > ");
            index = read_int();
            if (index >= 1 || index <= 3)
            {
                donate_bars[index]->func();
                printf("%s", donate_bars[index]->name);
                clear_donate(index);
            }
        }
    }

    return 0;
}