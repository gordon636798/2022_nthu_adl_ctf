#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

void init()
{
    setvbuf(stdin, NULL, _IONBF, 0);
    setvbuf(stdout, NULL, _IONBF, 0);
    setvbuf(stderr, NULL, _IONBF, 0);
}

void echo(char* buf)
{
    printf(buf); 
    puts("");
}

void saySomething(char* buf)
{
    read(0, buf, 0x10);
}

void bof(char* buf)
{
    puts("Show your BOF");
    read(0, buf, 0x100);
}


int main()
{

    init();

    char buf[0x10] = {0};
    write(1 , "> ", 2);
    saySomething(buf) ;
    echo(buf);
    bof(buf);

    return 0;
}
