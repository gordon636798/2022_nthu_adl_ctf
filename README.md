# 2022 攻防 HW1 writeup

## helloctf_again
### 考點
1. BOA
2. '\0'繞過strlen
3. stack alignment
### exploit
本題主要漏洞位置在於`sacnf("%s")`沒有對輸入的長度做限制，所以就存在buffer overflow的漏洞
```c=
scanf("%s", buf);
```
接著會使用`strlen`對於字串長度的檢查長度不能大於0x10否則會直接`exit(0)`
```c=
if (strlen(buf) > 0x10)
{
    puts("input toooooooo long");
    exit(0);
}
```
但是`strlen`只會算到字串到`\0`的長度，所以如果字串是`yes\0aaaaaaaaaaaaaa`則strlen出來的長度只有3而已，因此就可以繞過字串長度的檢查
而本題存在一個`magic`的function能夠開啟shell
```c=
void magic()
{
    system("/bin/sh");
    return;
}
```
如果成功將return address覆蓋成`magic`的起始位置如下圖的情況
![](https://i.imgur.com/LeWFjWM.png)
接著如果繼續往後走會segmentation fault，這邊是考點之一需要用gdb去追看他為甚麼會segmentation fault
用gdb往後追之後就能找到他發生segmentation fault的原因如下圖所示
![](https://i.imgur.com/dl2vCgo.png)
可以知道造成segmentation fault的主要原因是因為下面這個指令
```
movaps xmmword ptr [rsp + 0x50], xmm0
```
如果拿去google就能找到很多相關的文章在說明是為甚麼了，主要的原因就是因為stack沒有對齊所以造成在call system的時候會出問題，所以為了要讓stack能夠對齊主要兩個個做法
1. 在覆蓋return address的時候不要跳進magic的開頭讓他不要做function prologue
2. 在跳進magic前先塞一個ret的gadget幫他把stack對齊再跳進magic裡面

[exploit](helloctf_again/exploit.py)
## open_book_exam
### 考點
1. c語言fd
2. OOB (out of bound)
### exploit
本題主要在考C語言fd的特性，如果開啟一個檔案並沒有將它關閉則fd會一直存在並可重複對此fd做操作，因為0 1 2分別為stdin stdout stderr所以之後開啟的檔案會從3號fd開始使用
利用gdb來看全域變數的記憶體排列可以觀察到如下圖的結果
![](https://i.imgur.com/l7fobvc.png)
book_fd的位置在question的前面，而由於`write_ans`只有對正數做檢查沒有對負數做檢查，因此這裡存在OOB的漏洞
```c=
int choice = read_int();
if (choice > 4)
{
    puts("Invalid operation!");
    return;
}
```
因為book_fd在question的前面所以可以利用此OOB的漏洞來對book_fd做修改，因此如果一開始先使用`open_book`就將flag開啟就能拿到fd為3，接著在開啟任意一個檔案這時book_fd會變成4並將cur_book改為其他的檔案，由於前面已經利用`open_book`打開過flag並存在3號的fd，因此只要利用此OOB的漏洞把book_fd改成3接著利用`read_book`就能把flag的內容讀出來，因為第二次`open_book`已經把cur_book改掉就能通過`(strcmp(cur_book, "FLAG") == 0)`的檢查，並且透過漏洞把book_fd改成3就能把3號fd裡的內容讀出來


[exploit](open_book_exam/exploit.py)

## superchat

- checksec
``` s
    Arch:     amd64-64-little
    RELRO:    Partial RELRO
    Stack:    No canary found
    NX:       NX disabled
    PIE:      No PIE (0x400000)
    RWX:      Has RWX segments
```
沒有開任何防護

-  main function
```c=
undefined8 main(EVP_PKEY_CTX *param_1)
{
  undefined8 local_28;
  undefined8 local_20;
  undefined8 *local_10;
  
  init(param_1);
  init_seccomp();
  banner();
  local_28 = 0;
  local_20 = 0;
  local_10 = &local_28;
  read(0,&local_28,0x10);
  (*(code *)local_10)();
  return 0;
}

```
13 行表示輸入的大小只能進行 16 bytes 的輸入。

14~15 行表示將輸入的內容放在 buffer 中，並將內容當作機器碼執行。

- init_seccomp function
```c= 
void init_seccomp(void)
{
  undefined8 uVar1;
  
  uVar1 = seccomp_init(0x7fff0000);
  seccomp_rule_add(uVar1,0,0x3b,0);
  seccomp_load(uVar1);
  close(1);
  return;
}
```

第 6 行將 `execve` 加入至黑名單，表示寫 shellcode 時要避免使用到此 syscall。因此考慮 `open`, `read`以及`write`的方式將 flag 讀出。

第 8 行將 `stdout` 給 `close` 掉，表示最後 `write` 時不可從 `stdout` 讀出，需要將內容從 `stderr` 或 `stdin` 將內容輸出。



整理以上資訊步驟如下：
1. 第一次輸入時，寫入 `read` 擴大可輸入長度，並將機器碼控制在 16 bytes 內。
2. 第二次輸入時，寫入 `open`, `read` 以及 `write` 的 shellcode
3. 注意 `stdout` 已經被 `close`，須從其他 fd 輸出

:warning:第一步作法很多，也可以嘗試只改 rdi 就直接執行 `read` 也可以進入第二次輸入

[exploit](superchat/exploit.py)

## donate
### 考點
1. uaf
2. tcache
### exploit
本題主要漏洞位置在於 danate_bars 在被 free() 之後沒有將 donate_bars 的指標清除，因此仍可以對裡面的記憶體進行操作。

donate_bar 的結構為一個 funtion pointer 加上一個字串。
```c=
struct donate_bar
{
    void (*func)();
    char *name;
};
```

在執行第一個選項 add donate 時，會針對兩部分進行 malloc()，一個是結構本身，一個是 name 字串。

```c=
void add_donate(int i, int size)
{
    donate_bars[i] = (struct donate_bar *)malloc(sizeof(struct donate_bar));
    donate_bars[i]->func = say;
    donate_bars[i]->name = (char *)malloc(size);
}
```

在執行第二個選項時 say 時，則會執行 func 並 free 掉 donate_bar。

```c=
    donate_bars[index]->func();
    printf("%s", donate_bars[index]->name);
    clear_donate(index);
```

另外若使用 objdump 或是 gdb 可以看到程式內存在一個寫好的 magic funtion，會幫我們開出一個 shell。

```c=
    void magic_func()
    {
        system("/bin/sh");
    }
```
因為能寫入的點只有 name 的部分，因此這題的思路就是利用 tcache 的機制，當記憶體 free 掉後會保留在 tcache 中，當再次 malloc 相同大小的記憶體時，會優先拿 tcache 裡的記憶體來分配，若我們能讓新分配的記憶體的 name 部分取得過去分配給 function pointer 的部分，就可以重新使用 say 來呼叫我們在 name 寫入的 function。


基於以上資訊，一開始我們先對donate_bar1, donate_bar2 分別add_donate，因為我們希望待會 donate_bar3 在 malloc 時可以取得 A和 C 兩塊記憶體，所以這邊在分配 name 大小時不可以跟 A 和 C 相同，我分配 name 大小為 32 bytes。

![](https://i.imgur.com/NtZzVkG.png)

接著依序對 donate1, donate2 進行 say。此時 tcache 裡會有兩個 16 bytes 的 chunk(A、C) 和 兩個 32 bytes 的 chunk(B、D)。

接著我們在 add_donate 一個新的 donate_bar3，name 大小為 16 bytes，因為 tcache 剛好只有兩個 16 bytes 的 chunk，分別為 donate_bar1 和 donate_bar2 struct的記憶體(A、C)，此時 name 的前8個 bytes 剛好蓋掉 donate_bar1 的 func。

![](https://i.imgur.com/PTDjmUJ.png)


最後在 donate_bar3 的 name 裡填入 magic_func() 的 address，此時donate_bar1 裡的 func 就變為 magic_func() 的 address，再對 donate_bar1 進行一次 say 即可取得 flag。

[exploit](donate/exploit.py)
## easyBOF

- checksec
```s
    Arch:     amd64-64-little
    RELRO:    Partial RELRO
    Stack:    Canary found
    NX:       NX enabled
    PIE:      No PIE (0x400000)
```


- 透過 `file` 指令可以直接看到是 static link 的執行檔
``` shell
$ file easyBOF 
easyBOF: ELF 64-bit LSB executable, x86-64,
version 1 (GNU/Linux), statically linked, 
BuildID[sha1]=4c380fb7c70a4399300e8e3c82c2ff59cfb
81a25, for GNU/Linux 3.2.0, not stripped
```

透過以上資訊可以嘗試使用 ROP chain 的技巧，但注意到這題有開啟 `canary`，所以我們必須想辦法先 leak 出 `canary`

- main function
```c=
undefined8 main(void)
{
  long in_FS_OFFSET;
  undefined8 uStack40;
  undefined8 uStack32;
  long lStack16;
  
  lStack16 = *(long *)(in_FS_OFFSET + 0x28);
  init();
  uStack40 = 0;
  uStack32 = 0;
  write(1,&DAT_00495013,2);
  saySomething(&uStack40);
  echo(&uStack40);
  bof(&uStack40);
  if (lStack16 != *(long *)(in_FS_OFFSET + 0x28)) {
                    /* WARNING: Subroutine does not return */
    __stack_chk_fail();
  }
  return 0;
}

```

- echo function
```c=
void echo(char *param_1)
{
  printf(param_1);
  puts();
  return;
}
```
關鍵 function 在 `echo` 裡面，只有一個 `printf`。他將 `saySomething` 的輸入直接放進此處的 `printf`。因此可以利用 [`format string`](https://frozenkp.github.io/pwn/format_string/) 將有用資訊 leak 出。


- GDB info
```gdb
$rsp   : 0x00007fffffffe260


0x00007fffffffe260│+0x0000: 0x00000000004c0018  →  0x0000000000446ce0  →  <__strcpy_avx2+0> endbr64      ← $rsp
0x00007fffffffe268│+0x0008: 0x00007fffffffe280  →  "aaaaaaa\n"
0x00007fffffffe270│+0x0010: 0x00007fffffffe2a0  →  0x0000000000402f00  →  <__libc_csu_init+0> endbr64    ← $rbp
0x00007fffffffe278│+0x0018: 0x0000000000401eeb  →  <main+99> lea rax, [rbp-0x20]
0x00007fffffffe280│+0x0020: "aaaaaaa\n"  ← $rsi, $rdi
0x00007fffffffe288│+0x0028: 0x0000000000000000
0x00007fffffffe290│+0x0030: 0x0000000000000000
0x00007fffffffe298│+0x0038: 0x9d8d85adfe980500
```
透過 GDB 將斷點設在 `echo` 的 `printf` 並觀察　stack，`0x7fffffffe298` 處即是我們的目標 `canary`

透過計算可以得知我們需要 leak 第 13 個位置的值。所以我們可以輸出 `%13&p` 即得得到 `canary` 並輸出至 terminal


- bof function
```c=
void bof(void *param_1)
{
  puts(Show your BOF);
  read(0,param_1,0x100);
  return;
}

```
最後此處開了一個很大的可輸入空間，在此處塞入 ROP chain ，最後利用工具 `ROPgadget` 即可生出執行檔的 ROP chain

[exploit](easyBof/exploit.py)

## akukin
### 考點
1. PIE爆破
2. ROP
### exploit
一開始先看checksec可以看到除了canary以外其他的防護機制都打開了
![](https://i.imgur.com/Pbl1lkC.png)
在message這個function有一個很明顯BOA的漏洞，因為buf的大小只有30但可寫入的大小為0x30
```c=
void message()
{
    char buf[30];
    puts("What do you want to say?");
    puts("You can write some message to akukin");
    write(1, "> ", 2);
    read(0, buf, 0x30);
}
```
由於這個BOA的漏洞只能剛好蓋到return address而程式裡有另外一個function nothing裡面也有BOA的漏洞
```c=
void nothing()
{
    printf("Akukin find setvbuf %p  is it useful?\n", setvbuf);
    char secret[100];
    puts("Enter akukin secret: ");
    write(1, "> ", 2);
    read(0, secret, 0x100);
}
```
由於nothing裡面buffer overflow的漏洞的空間很大可以寫非常多東西，因此如果能夠跳進去`nothing`裡面就能夠透過這個BOA的漏洞來串ROP chain來get shell
這題主要的重點是PIE在做隨機時主要是以4k為單位來隨機，所以如果最低的12 bit是固定的，因此在覆蓋return address的時候只要寫2 byte就有1/16的機率能夠跳進去想要控制的地方
用objdump來反組譯這隻程式可以看到最低的12 bit為`26e`
![](https://i.imgur.com/qomwXBH.jpg)
程式執行起來的時候最低的2 byte會有`026e, 126e, 226e, 326e, 426e, 526e, 626e, 726e, 826e, 926e, a26e, b26e, c26e, d26e, e26e, f26e`這16種可能，所以就用個while迴圈一直去爆直到他成功跳進去`nothing`裡面，進到`nothing`裡面之後疊rop chain就能get shell了

[exploit](akukin/exploit.py)
## ign1010
### 考點
1. OOB
2. fmt
3. got hijack
### exploit
一開始先用checksec看這題的開了哪些防護機制，可以看到除了RELRO是partial RELRO以外其他防護機制都全開
![](https://i.imgur.com/URLfi2L.png)
可以看到game1010資料都是全域變數
```c=
struct game_info game1010[5];
```
而且`show_info`這個function因為只對正數做檢查沒有對負數做檢查因此存在OOB的漏洞
觀察BSS段上的資料可以看到如下圖
![](https://i.imgur.com/8ZxtWky.jpg)
因此如果輸入負數就可以透過`show_info`這個function把GOT表上的資料讀出來
而`edit_info`這個function則一樣沒有對負數做檢查因此存在OOB的漏洞，所以如果透過`edit_info`這個function能夠做到got hijack
而本題一開始能只能使用兩次各個功能，因此為了讓程式不要結束可以透過got hijack的方式來把exit hijack成main這樣如果執行到exit時就能夠回到main而不會讓程式結束掉，由於這題有開PIE因此需要先知道程式的起始位置才能夠找出main所在的位置
由於linux lazy binding機制的關係，因此如果當一個libc的function還沒被執行到的時候他GOT表上所存的位置為PLT的位置，因此如果能leak出還沒被執行過的libc function就能找出code base就能找出main的位置
如下圖所示由於exit在程式一開始還沒被執行到所以他所存的位置是`0x0000555555555110`
![](https://i.imgur.com/LAKjR0n.png)
所以如果利用`show_info`這個function就能夠把leak出這個位置並回推main的位置，接著透過`edit_info`來把exit的GOT修改成main就能夠在程式執行到exit的時候再回到main
由於在main存在如下列的一段程式
```c=
for (i = 0; i < 5; i++) 
{
    puts(game1010[i].name);
}
```
game1010[i].name是可控的，再加上程式一開始就會把flag開啟放在stack上所以如果把puts的got hijack成printf，因為puts被hijack成printf因此就會存在format string的漏洞，接著只要去觀察flag所在的位置把game1010[i].name寫成%?$p之後的就能夠利用format string的漏洞來把flag讀出來

[exploit](ign1010/exploit.py)

