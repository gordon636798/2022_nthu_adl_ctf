#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

void init()
{
    setvbuf(stdin, NULL, _IONBF, 0);
    setvbuf(stdout, NULL, _IONBF, 0);
    setvbuf(stderr, NULL, _IONBF, 0);
}

int read_int()
{
    char buf[0x10];
    read(0, buf, 0x10);
    return atoi(buf);
}

void nothing()
{
    printf("Akukin find setvbuf %p  is it useful?\n", setvbuf);
    char secret[100];
    puts("Enter akukin secret: ");
    write(1, "> ", 2);
    read(0, secret, 0x100);
}

void info()
{
    puts("Here is the info of akukin");
    puts("Website: https://akukin.jp");
    puts("Boss: MinatoAqua");
    puts("Aqua's Toutube channel: https://www.youtube.com/channel/UC1opHUrw8rvnsadT-iGp7Cg");
    puts("Aqua's Twitter: https://twitter.com/minatoaqua");
}

void message()
{
    char buf[30];
    puts("What do you want to say?");
    puts("You can write some message to akukin");
    write(1, "> ", 2);
    read(0, buf, 0x30);
}

void menu()
{
    puts("What function do you want?");
    puts("1. info");
    puts("2. message");
    write(1, "> ", 2);
}

int main()
{
    init();
    puts("Welcome to akukin!!");
    while (1)
    {
        menu();
        int choice = read_int();
        puts("");
        switch (choice)
        {
        case 1:
            info();
            break;
        case 2:
            message();
            break;
        default:
            break;
        }
        puts("");
    }
}