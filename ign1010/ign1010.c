#include <fcntl.h>
#include <seccomp.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#define __init __attribute__((constructor))

// struct
struct game_info {
    char name[0x28];
    char company[0x28];
    char wiki[0x8];
    char comment[0x28];
};

// global variable
struct game_info game1010[5];

// function
void init_msg() {
    puts("ooooo   ooooooo8  oooo   oooo    oo    ooooooo           o88   oo    ooooooo");
    puts(" 888  o888    88   8888o  88   o888  o888  o888o        o88  o888  o888  o888o");
    puts(" 888  888    oooo  88 888o88    888  888  8  888      o88     888  888  8  888");
    puts(" 888  888o    88   88   8888    888  888o8  o888    o88       888  888o8  o888");
    puts("o888o  888ooo888  o88o    88   o888o   88ooo88    o88        o888o   88ooo88");
    puts("                                                 o88");
}

void init_info() {
    strcpy(game1010[0].name, "Pokémon Red and Blue");
    strcpy(game1010[0].company, "GAME FREAK");
    strcpy(game1010[0].wiki, "fXgkcx");
    memset(game1010[0].comment, 0, 0x28);
    strcpy(game1010[1].name, "The Legend of Zelda: Breath of the Wild");
    strcpy(game1010[1].company, "Nintendo EPD");
    strcpy(game1010[1].wiki, "fQUQ6x");
    memset(game1010[1].comment, 0, 0x28);
    strcpy(game1010[2].name, "Red Dead Redemption 2");
    strcpy(game1010[2].company, "Rockstar Games");
    strcpy(game1010[2].wiki, "fXTjBx");
    memset(game1010[2].comment, 0, 0x28);
    strcpy(game1010[3].name, "The Last of Us Part II");
    strcpy(game1010[3].company, "Naughty Dog");
    strcpy(game1010[3].wiki, "ftqkXx");
    memset(game1010[3].comment, 0, 0x28);
    strcpy(game1010[4].name, "Elden Ring");
    strcpy(game1010[4].company, "FromSoftware");
    strcpy(game1010[4].wiki, "f7vTyx");
}

__init void init() {
    setvbuf(stdin, NULL, _IONBF, 0);
    setvbuf(stdout, NULL, _IONBF, 0);
    setvbuf(stderr, NULL, _IONBF, 0);
    scmp_filter_ctx ctx = seccomp_init(SCMP_ACT_ALLOW);
    seccomp_rule_add(ctx, SCMP_ACT_KILL, SCMP_SYS(execve), 0);
    seccomp_rule_add(ctx, SCMP_ACT_KILL, SCMP_SYS(execveat), 0);
    seccomp_load(ctx);
    seccomp_release(ctx);
    init_info();
    init_msg();
}

void show_info() {
    int idx = 0;
    puts("Enter which info to read (index should between 0-4)");
    write(1, "idx> ", 5);
    scanf("%d", &idx);

    if (idx >= 5) {
        puts("Invalid index!!");
        return;
    }

    printf("name: %s\n", game1010[idx].name);
    printf("company: %s\n", game1010[idx].company);
    printf("wiki: https://ppt.cc/%s\n", game1010[idx].wiki);
    printf("comment: %s\n", game1010[idx].comment);
}

void edit_info() {
    int idx = 0;
    int choice = 0;
    char buf[0x28];
    memset(buf, 0, sizeof(buf));
    puts("Enter which game to edit (index should between 0-4)");
    write(1, "idx> ", 5);
    scanf("%d", &idx);

    if (idx >= 5) {
        puts("Invalid index!!");
        return;
    }

    printf("Select game: %s.\n", game1010[idx].name);
    puts("Enter which info to edit");
    puts("1.name");
    puts("2.company");
    puts("3.wiki");
    puts("4.comment");
    write(1, "> ", 2);
    scanf("%d", &choice);

    if (choice <= 0 || choice > 4) {
        puts("Wrong argument");
        return;
    }

    write(1, "Content: ", 9);
    switch (choice) {
        case 1:
            read(0, buf, 0x28);
            strcpy(game1010[idx].name, buf);
            break;
        case 2:
            read(0, buf, 0x28);
            strcpy(game1010[idx].company, buf);
            break;
        case 3:
            read(0, buf, 0x8);
            strcpy(game1010[idx].wiki, buf);
            break;
        case 4:
            read(0, buf, 0x28);
            strcpy(game1010[idx].comment, buf);
            break;
        default:
            puts("Invalid argument");
    }
    puts("Edit success");
}

int main() {
    int i, cnt = 2;
    char text[40] = {0};
    i = open("/home/ign1010/flag", R_OK);
    read(i, text, 40);

    while (cnt--) {
        puts("1. show game info");
        puts("2. edit game info");
        write(1, "> ", 2);
        scanf("%d", &i);
        switch (i) {
            case 1:
                show_info();
                break;
            case 2:
                edit_info();
                break;
            default:
                puts("Invalie choice!!");
        }
        puts("");
    }

    puts("10/10 games:");
    for (i = 0; i < 5; i++) {
        puts(game1010[i].name);
    }

    puts("--------------------------------------");
    puts("The Last of Us Part II is a golf game.");
    exit(0);
}
