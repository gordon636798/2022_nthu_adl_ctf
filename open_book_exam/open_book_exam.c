#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>

int book_fd = -1;
int questions[4] = {0, 0, 0, 0};
char *cur_book = NULL;

void init()
{
    setvbuf(stdin, NULL, _IONBF, 0);
    setvbuf(stdout, NULL, _IONBF, 0);
    setvbuf(stderr, NULL, _IONBF, 0);
}

int read_int()
{
    char buf[0x10];
    read(0, buf, 0x10);
    return atoi(buf);
}

void menu()
{
    puts("what do you want to do?");
    puts("1. open book");
    puts("2. read book");
    puts("3. write answer");
    puts("4. subit answer");
}

void open_book()
{
    puts("which book to open?");
    puts("1. Chinese");
    puts("2. Math");
    puts("3. Science");
    puts("4. English");
    puts("5. FLAG");
    write(1, "> ", 2);
    int book = read_int();
    if (book == 1)
    {
        book_fd = open("/home/open_book_exam/books/Chinese", O_RDONLY);
        if(book_fd == -1)
        {
            puts("open book error");
            exit(0);
        }
        cur_book = "Chinese";
    }
    else if (book == 2)
    {
        book_fd = open("/home/open_book_exam/books/Math", O_RDONLY);
        if(book_fd == -1)
        {
            puts("open book error");
            exit(0);
        }
        cur_book = "Math";
    }
    else if (book == 3)
    {
        book_fd = open("/home/open_book_exam/books/Science", O_RDONLY);
        if(book_fd == -1)
        {
            puts("open book error");
            exit(0);
        }
        cur_book = "Science";
    }
    else if (book == 4)
    {
        book_fd = open("/home/open_book_exam/books/English", O_RDONLY);
        if(book_fd == -1)
        {
            puts("open book error");
            exit(0);
        }
        cur_book = "English";
    }
    else if (book == 5)
    {
        book_fd = open("/home/open_book_exam/books/FLAG", O_RDONLY);
        if(book_fd == -1)
        {
            puts("open book error");
            exit(0);
        }
        cur_book = "FLAG";
    }
    else
    {
        puts("Invalid operation!!");
    }
}

void read_book()
{
    if (book_fd == -1)
    {
        puts("open one book before read it");
        return;
    }

    if (strcmp(cur_book, "FLAG") == 0)
    {
        puts("you can't read the content of FLAG!!");
        return;
    }
    else
    {
        puts("here is the content of book: ");
        char buf[64];
        int ret = read(book_fd, buf, 64);
        buf[ret] = '\x00';
        printf("%s\n", buf);
    }
}

void write_ans()
{
    puts("Q1. 1+1 = ?");
    puts("Q2. 100*100 = ?");
    puts("Q3. x-7 = 87, x=?");
    puts("Q4. 9/2+1 = ?");
    puts("which question do you want to write? (1~4)");
    write(1, "> ", 2);
    int choice = read_int();
    if (choice > 4)
    {
        puts("Invalid operation!");
        return;
    }

    write(1, "ans: ", 5);
    int ans = read_int();
    questions[choice - 1] = ans;
}

void submit_ans()
{
    puts("you have submitted answer bye~");
    exit(0);
}

int main()
{
    init();
    while (1)
    {
        int choice = 0;
        menu();
        write(1, "> ", 2);
        choice = read_int();
        puts("");
        switch (choice)
        {
        case 1:
            open_book();
            break;
        case 2:
            read_book();
            break;
        case 3:
            write_ans();
            break;
        case 4:
            submit_ans();
            break;
        default:
            puts("Invalid operation");
            break;
        }
        puts("");
    }
}